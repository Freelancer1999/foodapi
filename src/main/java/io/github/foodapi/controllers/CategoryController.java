package io.github.foodapi.controllers;

import io.github.foodapi.models.Category;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/categories")
@Api(value = "Category APIs")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @ApiOperation(value = "Lấy toàn bộ danh sách thể loại", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getAllCategories")
    public List<Category> getAllCategories(){
        return categoryService.getAllCategories();
    }


    @ApiOperation(value = "Xem chi tiết thể loại theo Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getCategory/{id}")
    public Optional<Category> getCategory(@PathVariable Long id) {
        return categoryService.getCategory(id);
    }


    @ApiOperation(value = "Tạo mới thể loại đồ ăn", response = List.class)
    @ResponseBody
    @PostMapping("/createCategory")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<?> createCategory(@Valid @RequestBody Category newCategory, @CurrentUser UserPrinciple currentUser ) {
        this.categoryService.createCategory(newCategory, currentUser);
        return new ResponseEntity<>(newCategory, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Xóa một thể loại", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/deleteCategory/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteCategory(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        categoryService.deleteCategory(id, currentUser);
    }

    @ApiOperation(value = "Cập nhật một category food", response = List.class)
    @ResponseBody
    @PostMapping("/editCategory/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public Category editCategory(@Valid @RequestBody Category editCategory, @PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser ) {
        return categoryService.editCategory(id, editCategory,currentUser);
    }
}
