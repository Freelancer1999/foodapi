package io.github.foodapi.controllers;

import io.github.foodapi.models.DTO.NewsSearch;
import io.github.foodapi.models.News;
import io.github.foodapi.services.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/news")
@Api(value = "News APIs")
public class NewsController {
    @Autowired
    private NewsService newsService;

    @ApiOperation(value = "Tim kiem danh sach bai viet", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @PostMapping("/list")
    public ResponseEntity<?> searchCustomerList(@RequestBody NewsSearch newsSearch) {
        Page<News> listCustomers = newsService.findListNews(newsSearch);
        return new ResponseEntity<>(listCustomers, HttpStatus.OK);
    }

}
