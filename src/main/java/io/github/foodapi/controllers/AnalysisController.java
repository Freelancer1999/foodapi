//package io.github.foodapi.controllers;
//
//import io.github.foodapi.models.Category;
//import io.github.foodapi.payload.response.AnalysisOrderResponse;
//import io.github.foodapi.payload.response.AnalysisResponse;
//import io.github.foodapi.services.AnalysisService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@CrossOrigin(origins = "*", maxAge = 3600)
//@RestController
//@RequestMapping("/api/analysis")
//@Api(value = "Analysis APIs")
//public class AnalysisController {
//
//    @Autowired
//    AnalysisService analysisService;
//
//    @ApiOperation(value = "Số nhân viên hiện tại trong nhà hàng", response = List.class)
//    @ResponseStatus(code = HttpStatus.OK)
//    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
//    @GetMapping(value="/countAllUsers")
//    public ResponseEntity<Integer> countAllUsers(){
//        Integer analysis =  analysisService.countAllUser();
//        return new ResponseEntity<>(analysis, HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "Số món ăn đang kinh doanh trong nhà hàng", response = List.class)
//    @ResponseStatus(code = HttpStatus.OK)
//    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
//    @GetMapping(value="/countAllFoods")
//    public ResponseEntity<Integer> countAllFoods(){
//        Integer analysis =  analysisService.countAllFoods();
//        return new ResponseEntity<>(analysis, HttpStatus.OK);
//    }
//
//
//
//}
