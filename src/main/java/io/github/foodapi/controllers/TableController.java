package io.github.foodapi.controllers;

import io.github.foodapi.models.RestaurantTable;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.TableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/table")
@Api(value = "Table APIs")
public class TableController {
    @Autowired
    TableService tableService;

    @ApiOperation(value = "Lấy toàn bộ danh sách bàn của nhà hàng", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/restaurant-tables")
    public List<RestaurantTable> getAllRestaurantTables() {
        return tableService.getAllRestaurantTables();
    }

    @ApiOperation(value = "Lấy toàn bộ danh sách bàn theo số chỗ ngồi", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/restaurant-tables/seating/{number}" )
    public List<RestaurantTable> getTablesBySeating(@PathVariable int number){
        return tableService.getTablesBySeating(number);
    }

    @ApiOperation(value = "Nhận được danh sách các bàn có sẵn trong ngày", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/restaurant-tables/availableondate/{date}" )
    public List<RestaurantTable> getTablesAvailableOnDate(@PathVariable String date){
        return tableService.getTablesAvailableOnDate(date);
    }

    @ApiOperation(value = "Thêm mới bàn ăn", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/createTable")
    @PreAuthorize("hasRole('ADMIN')")
    public RestaurantTable createTable(@Valid @RequestBody RestaurantTable newTable, @CurrentUser UserPrinciple currentUser) {
        return tableService.createTable(newTable, currentUser);
    }


    @ApiOperation(value = "Cập nhật một bàn ăn", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/editTable/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public RestaurantTable editTable(@Valid @RequestBody RestaurantTable editTable, @PathVariable(name = "id") Long id,
                               @CurrentUser UserPrinciple currentUser) {
        return tableService.editTable(id, editTable, currentUser);

    }

    @ApiOperation(value = "Xóa một bàn ăn", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @DeleteMapping("/deleteTable/{id}")
    public void deleteTable(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        tableService.deleteTable(id, currentUser);
    }

    @ApiOperation(value = "Xem chi tiết bàn ăn theo Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/getTable/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public Optional<RestaurantTable> getAccount(@PathVariable(name = "id") Long id) {
        return tableService.getTable(id);
    }

    @ApiOperation(value = "Update status table", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/editStatusTable")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<?> updateTableStatus(@Param("id") Long id, @Param("status") Boolean status) {
        tableService.updateTableStatus(status, id);
        return new ResponseEntity<>("Update status success", HttpStatus.OK);
    }

}
