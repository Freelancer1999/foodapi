package io.github.foodapi.controllers;

import io.github.foodapi.models.Transaction;
import io.github.foodapi.services.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/transactions")
@Api(value = "Transactions APIs")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @ApiOperation(value = "Lấy toàn bộ danh sách giao dịch", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping("/getAllTransactions")
    public List<Transaction> getAllTransactions(){
        return transactionService.getAllTransactions();
    }


    @ApiOperation(value = "Lấy toàn bộ danh sách giao dịch theo ngày", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/date/{date}")
    public List<Transaction> getAllTransactionsForDate(@PathVariable String date) {
        return transactionService.getAllTransactionsForDate(date);
    }

    @ApiOperation(value = "Lấy toàn bộ danh sách giao dịch theo Booking Id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping(value="/booking-id/{bookingId}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public List<Transaction> getTransactionsByBookingId(@PathVariable Long bookingId){
        return transactionService.getTransactionsByBookingId(bookingId);
    }


    @ApiOperation(value = "Lấy toàn bộ danh sách giao dịch theo mã khách hàng", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/customer-id/{customerId}")
    public List<Transaction> getTransactionsByCustomerId(@PathVariable Long customerId){
        return transactionService.getTransactionsByCustomerId(customerId);
    }

    @ApiOperation(value = "Lấy toàn bộ danh sách giao dịch theo tên khách hàng", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/customer-name/{customerName}")
    public List<Transaction> getTransactionsByCustomerName(@PathVariable String customerName){
        return transactionService.getTransactionsByCustomerName(customerName);
    }
}
