package io.github.foodapi.controllers;

import io.github.foodapi.models.Food;
import io.github.foodapi.payload.request.FoodRequest;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.FoodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/food")
@Api(value = "Food APIs")
public class FoodController {

    @Autowired
    FoodService foodService;

    @ApiOperation(value = "Lấy toàn bộ danh sách món ăn", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getAllFoods")
    public ResponseEntity<List<Food>> getAllFoods(){
        List<Food> foods = foodService.getAllFoods();
        return new ResponseEntity<>(foods, HttpStatus.OK);
    }

    @ApiOperation(value = "Xem chi tiết món ăn theo Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getFood/{id}")
    public ResponseEntity<Optional<Food>> getFood(@PathVariable Long id) {
        Optional<Food> food =  foodService.getFood(id);
        if(food  != null) {
            return new ResponseEntity<>(food, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Xem chi tiết món ăn theo loại món ăn", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getFoodByCategoryId/{id}")
    public List<Food> getFoodByCategoryId(@PathVariable("id")  Long categoryId) {
        return foodService.getFoodByCategoryId(categoryId);
    }

    @ApiOperation(value = "Tạo mới một đồ ăn", response = List.class)
    @ResponseBody
    @PostMapping(value = "/createFood" )
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<FoodRequest> createFood(@Valid @RequestBody FoodRequest newFood, @CurrentUser UserPrinciple currentUser )  {
        this.foodService.createFood(newFood, currentUser);
        return new ResponseEntity<>(newFood, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Xóa một món ăn", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/deleteFood/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteFood(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        foodService.deleteFood(id, currentUser);
    }

    @ApiOperation(value = "Cập nhật một đồ ăn", response = List.class)
    @ResponseBody
    @PutMapping("/editFood/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Food> editFood(@Valid @PathVariable(name = "id") Long id, @RequestBody FoodRequest editFood, @CurrentUser UserPrinciple currentUser ) {
        Food foods = this.foodService.editFood(id, editFood, currentUser);
        return new ResponseEntity<>(foods, HttpStatus.OK);
    }
}
