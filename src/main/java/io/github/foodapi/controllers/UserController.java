package io.github.foodapi.controllers;

import io.github.foodapi.models.User;
import io.github.foodapi.payload.request.UserRequest;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
@Api(value = "User APIs")
public class UserController {
    @Autowired
    UserService userService;


    @ApiOperation(value = "Lấy danh sách tất cả user ", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/getAllUser")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<User> getAllUser() {
        return userService.getAllUsers();
    }


    @ApiOperation(value = "Xem chi tiết user theo id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/getUser/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Optional<User> getUser(@PathVariable(name = "id") Long id) {
        return userService.getUser(id);
    }


    @ApiOperation(value = "Thêm mới một user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseBody
    @PostMapping("/createUser")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<?> createAccount(@Valid @RequestBody UserRequest userRequest, @CurrentUser UserPrinciple currentUser) {
        User user = this.userService.createUser(userRequest, currentUser);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }


    @ApiOperation(value = "Cập nhật một user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/editUser/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public User editUser(@Valid @RequestBody User editUser, @PathVariable(name = "id") Long id,
                               @CurrentUser UserPrinciple currentUser) {
        return userService.editUser(id, editUser, currentUser);
    }


    @ApiOperation(value = "Xóa một user", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy")
    })
    @ResponseBody
    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public void deleteAdmin(@PathVariable(name = "id") Long id,  @CurrentUser UserPrinciple currentUser) {
            userService.deleteUser(id, currentUser);

    }
}
