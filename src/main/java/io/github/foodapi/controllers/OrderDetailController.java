package io.github.foodapi.controllers;

import io.github.foodapi.models.OrderDetail;
import io.github.foodapi.payload.request.AddFoodRequest;
import io.github.foodapi.payload.response.*;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.OrderDetailService;
import io.github.foodapi.services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/orderDetail")
@Api(value = "order Detail APIs")
public class OrderDetailController {

    @Autowired
    OrderDetailService orderDetailService;

    @Autowired
    OrderService orderService;

    @ApiOperation(value = "Lấy tất cả số lượng món ăn theo order id", response = List.class)
    @ResponseBody
    @GetMapping("/getAllOrderDetail/{orderId}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<OrderFoodResponse>> getOrderDetailList(@PathVariable(name = "orderId") Long orderId) {
        List<OrderFoodResponse> orders =  this.orderDetailService.getOrderDetails(orderId);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    } 

    @ApiOperation(value = "Thêm mới mới food cho order", response = List.class)
    @ResponseBody
    @PostMapping("/addFood")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<OrderDetail>> createOrder(@Valid @RequestBody AddFoodRequest newOrder, @CurrentUser UserPrinciple currentUser ) {
        List<OrderDetail> orderDetails =  this.orderDetailService.createOrderDetail(newOrder, currentUser);


        return new ResponseEntity<>(orderDetails, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Thống kê total price theo food", response = List.class)
    @ResponseBody
    @GetMapping("/getPriceByFood")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<AnalysisResponse>> getPriceByFood(){
        List<AnalysisResponse> orderDetailList = this.orderDetailService.getPriceByFood();
        return new ResponseEntity<>(orderDetailList, HttpStatus.OK);
    }

    @ApiOperation(value = "Thống kê doanh thu theo tháng", response = List.class)
    @ResponseBody
    @GetMapping("/getAnalysisOrder")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<AnalysisOrderResponse>> getAnalysisOrder(){
        List<AnalysisOrderResponse> orderDetailList = this.orderDetailService.getAnalysisOrder();
        return new ResponseEntity<>(orderDetailList, HttpStatus.OK);
    }

    @ApiOperation(value = "Thống kê doanh thu theo ngày", response = List.class)
    @ResponseBody
    @GetMapping("/getAnalysisOrderByDay")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<AnalysisOrderByDayResponse>> getAnalysisOrderByDay(){
        List<AnalysisOrderByDayResponse> list = this.orderDetailService.getAnalysisOrderByDay();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "Thống kê doanh thu theo customer trong thang", response = List.class)
    @ResponseBody
    @GetMapping("/getTotalPriceByCustomer")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<List<AnalysisTotalPriceByCustomer>> getTotalPriceByCustomer() {
        List<AnalysisTotalPriceByCustomer> list = this.orderDetailService.getAnalysisTotalPriceForCustomerByMonth();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
