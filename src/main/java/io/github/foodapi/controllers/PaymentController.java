package io.github.foodapi.controllers;

import io.github.foodapi.models.Payment;
import io.github.foodapi.services.PaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/payment")
@Api(value = "Payment APIs")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @ApiOperation(value = "Lưu hình thức thanh toán", response = List.class)
    @PostMapping(value = "/createPayment")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public void savePayment(@RequestBody Payment payment){
        this.paymentService.savePayment(payment);
    }

    @ApiOperation(value = "Lấy tất cả danh sách thanh toán", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value = "/getAllPayments")
    public ResponseEntity<List<Payment>> findAllPayment(){
        List<Payment> payments =  this.paymentService.findALl();
        return new ResponseEntity<>(payments, HttpStatus.OK);
    }
}
