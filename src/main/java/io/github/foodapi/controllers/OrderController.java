package io.github.foodapi.controllers;

import io.github.foodapi.models.Order;
import io.github.foodapi.payload.request.OrderRequest;
import io.github.foodapi.payload.response.OrderBookingResponse;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.BookingService;
import io.github.foodapi.services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/order")
@Api(value = "Orders APIs")
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    BookingService bookingService;


    @ApiOperation(value = "Lấy tất cả danh sách order", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getAllOrders")
    public List<Order> getAllOrders() {
        return orderService.getAllOrders();
    }


    @ApiOperation(value = "Xem chi tiết order", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getOrder/{id}")
    public Optional<Order> getOrder(@PathVariable Long id) {
        return orderService.getOrder(id);
    }


    @ApiOperation(value = "Tạo mới một Order", response = List.class)
    @ResponseBody
    @PostMapping("/createOrder")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Order> createOrder(@Valid @RequestBody OrderRequest newOrder, @CurrentUser UserPrinciple currentUser ) {
        Order order = this.orderService.createOrder(newOrder, currentUser);
        if (order != null) {
            this.bookingService.updateStatusOrder(newOrder.getCustomer().getId());
        }
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Xóa một Order", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/deleteOrder{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteOrder(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        orderService.deleteOrder(id, currentUser);
    }

    @ApiOperation(value = "Lấy orders theo Customer Id danh cho Web", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/web/getAll/")
    public ResponseEntity<List<OrderBookingResponse>> getOrderCustomer() {
        List<OrderBookingResponse> orders =  orderService.findByOrder();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @ApiOperation(value = "Lấy orders theo Customer Id danh cho Moble", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/mob/customer/{customerId}")
    public ResponseEntity<Order> getOrderByCustomer(@PathVariable Long customerId) {
        Order orders =  orderService.findOrderByCustomer(customerId);
        if (orders != null ) {
            return new ResponseEntity<>(orders, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Cập nhật một đơn hàng", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/editOrder/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<String> editOrder(@Valid @RequestBody Order editOrder, @PathVariable(name = "id") Long id,
                                            @CurrentUser UserPrinciple currentUser) {
        Optional<Order> order = this.orderService.getOrder(id);
        if (order != null) {
            this.orderService.editOrder(order.get().getId(), editOrder, currentUser);
            return new ResponseEntity<>("Order updated successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Order id not found",HttpStatus.NOT_FOUND);
    }

}
