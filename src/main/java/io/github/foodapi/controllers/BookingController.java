package io.github.foodapi.controllers;

import io.github.foodapi.models.Booking;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.BookingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/bookings")
@Api(value = "Bookings APIs")
public class BookingController {
    @Autowired
    BookingService bookingService;


    @ApiOperation(value = "Lấy tất cả danh sách booking", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getAllBooking")
    public List<Booking> getAllBooking() {
        return bookingService.getAllBooking();
    }

    @ApiOperation(value = "Xem chi tiết một booking", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Optional<Booking>> getBookingById(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        Optional<Booking> booking = bookingService.getBookingById(id, currentUser);
        return new ResponseEntity<>(booking, HttpStatus.OK);
    }

    @ApiOperation(value = "Lấy tất cả booking theo Ngày giờ và mã khách hàng", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/date/{date}/time/{time}/customer/{customerId}")
    public List<Booking> getBookingByDateTimeAndCustomerId(@PathVariable String checkin, @PathVariable String checkout, @PathVariable Long customerId) {
        return bookingService.getBookingByDateTimeAndCustomerId(checkin, checkout, customerId);
    }


    @ApiOperation(value = "Tạo mới booking theo  mã khách hàng", response = List.class)
    @ResponseBody
        @PostMapping("/createBooking")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Booking> createbooking(@Valid @RequestBody Booking newBooking,  @CurrentUser UserPrinciple currentUser ) {
        Booking booking = this.bookingService.createBooking(newBooking, currentUser);
        return new ResponseEntity<>(booking, HttpStatus.CREATED);
    }


    @ApiOperation(value = "Cập nhật một booking", response = List.class)
    @ResponseBody
    @PostMapping("/editBooking/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public Booking editbooking(@Valid @RequestBody Booking editBooking, @PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser ) {
        return bookingService.editBooking(editBooking, id, currentUser);
    }

    @ApiOperation(value = "Xóa một booking", response = List.class)
    @ResponseBody
    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/deleteBooking/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteBooking(@PathVariable(name = "id") Long id, @CurrentUser UserPrinciple currentUser) {
        bookingService.deleteBooKing(id, currentUser);
    }

    @ApiOperation(value = "Lấy booking theo Table Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/table/{tableId}")
    public ResponseEntity<Booking> getBookingByTableId( @PathVariable Long tableId) {
        Booking booking =  bookingService.findByBookingByTableId(tableId);
        return new ResponseEntity<>(booking, HttpStatus.OK);
    }

    @ApiOperation(value = "Lấy booking theo Customer Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/customer/{customerId}")
    public List<Booking> getBookingByCustomerId( @PathVariable Long customerId) {
        return bookingService.findBookingByCustomerId(customerId);
    }
}
