package io.github.foodapi.controllers;

import io.github.foodapi.models.Customer;
import io.github.foodapi.security.CurrentUser;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/customers")
@Api(value = "Customers APIs")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @ApiOperation(value = "Lấy toàn bộ danh sách Khách hàng", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/getAllCustomers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> customers = this.customerService.getAllCustomers();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }


    @ApiOperation(value = "Lấy toàn bộ danh sách Khách hàng theo tên", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/name/{name}")
    public List<Customer> getCustomerByName(@PathVariable String name){
        return customerService.getCustomerByName(name);
    }

    @ApiOperation(value = "Lấy toàn bộ danh sách Khách hàng theo Id", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/{id}")
    public List<Customer> getCustomerById(@PathVariable Long id){
        return customerService.getCustomerById(id);
    }


    @ApiOperation(value = "Lấy toàn bộ khách hàng đặt hàng theo số lượt booking", response = List.class)
    @ResponseStatus(code = HttpStatus.OK)
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    @GetMapping(value="/frequency")
    public List<Customer> getAllCustomersOrderByCountBookings(){
        return customerService.getAllCustomersOrderByCountBookings();
    }

    @ApiOperation(value = "Thêm một khách hàng", response = List.class)
    @ResponseBody
    @PostMapping("/createCustomer")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer newCustomer, @CurrentUser UserPrinciple currentUser ) {
        Customer customers = this.customerService.creatCustomer(newCustomer, currentUser);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @ApiOperation(value = "Cập nhật một khách hàng", response = List.class)
    @ResponseBody
    @PutMapping("/editCustomer/{id}")
    @PreAuthorize("hasRole('USER')  or hasRole('ADMIN')")
    public ResponseEntity<Customer> editCustomer(@Valid @PathVariable(name = "id") Long id, @RequestBody Customer editCustomer, @CurrentUser UserPrinciple currentUser ) {
        Customer customers = this.customerService.editCustomer(id, editCustomer, currentUser);
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

}
