package io.github.foodapi.repository;

import io.github.foodapi.models.Booking;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface BookingRepositoryCustom {
    List<Booking> getBookingByDateTimeAndCustomerId(String checkin, String checkout, Long customerId);

    List<Booking> getBookingsByCustomerId(Long customerId);
}
