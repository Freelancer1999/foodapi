package io.github.foodapi.repository;

import io.github.foodapi.models.RestaurantTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface RestaurantTableRepository extends JpaRepository<RestaurantTable, Long> {
    List<RestaurantTable> findTablesBySeating(int number);
    List<RestaurantTable> getTablesAvailableOnDate(String date);
    List<RestaurantTable> findAll();

//    @Query(value = "Update restaurant_tables t SET t.status=?1 WHERE t.id=?2", nativeQuery = true)
//    RestaurantTable updateTableStatus(Boolean status, Long id);

    @Modifying
    @Query(value = "update restaurant_tables set status = :status WHERE id = :id",nativeQuery = true)
    void setStatusTable(@Param("status") Boolean status, @Param("id") Long id);


}
