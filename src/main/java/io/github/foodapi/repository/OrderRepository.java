package io.github.foodapi.repository;

import io.github.foodapi.models.Order;
import io.github.foodapi.payload.response.OrderResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {


    @Query(value = "SELECT o.id, o.order_time, c.name, c.phone,b.notes, b.number_in_party\n" +
            "FROM orders o INNER JOIN customers c INNER JOIN bookings b", nativeQuery = true)
    List<Object[]> findByOrder();

    @Query(value = "SELECT * FROM orders INNER JOIN customers ON orders.customer_id = customers.id\n" +
            "WHERE customer_id =?1", nativeQuery = true)
    Order findByOrderByCustomer(Long customerId);

    @Query(value = "SELECT sum(o.quantity) FROM order_detail o WHERE order_id = :orderId;", nativeQuery = true)
    Integer sumQuantityTotalOrder(Long orderId);

    @Query(value = "SELECT sum(od.price)  from orders inner join customers c on orders.customer_id = c.id\n" +
            "inner join order_detail od on orders.id = od.order_id where c.id = :customerId", nativeQuery = true)
    Integer sumPriceTotalOrder(Long customerId);

    @Query(value = "UPDATE orders o set o.total_price = (SELECT sum(od.price)  from order_detail od inner join customers c where c.id = :customerId)\n" +
            "where o.customer_id = :customerId;", nativeQuery = true)
    Integer sumTotalPriceByCustomerId( Long customerId);


}
