package io.github.foodapi.repository;

import io.github.foodapi.models.RestaurantTable;

import java.util.List;

public interface RestaurantTableRepositoryCustom {
    List<RestaurantTable> getTablesAvailableOnDate(String date);
}
