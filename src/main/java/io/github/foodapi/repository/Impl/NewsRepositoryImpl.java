package io.github.foodapi.repository.Impl;

import io.github.foodapi.models.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class NewsRepositoryImpl {
    @PersistenceContext
    private EntityManager entityManager;

    public Page<News> searchNews(News news, Pageable pageable) {
        List<News> listNews = new ArrayList<>();
        Page<News> pageNews = new PageImpl<>(listNews, pageable, 0);
        Map<String, Object> parameters = new HashMap<>();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT * from news");
            sb.append(" WHERE 1=1");

            if (news.getId() != null) {
                sb.append(" AND id = :newsId");
                parameters.put("newsId", news.getId());
            }

            if (news.getTitle() != null) {
                sb.append(" AND title like :title");
                parameters.put("title", "%" + news.getTitle().trim() + "%");
            }


            Query query = entityManager.createNativeQuery(sb.toString(), News.class);

            int total = 0;
            total = query.getResultList().size();
            if (pageable != null) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            listNews= query.getResultList();
            pageNews = new PageImpl<>(listNews, pageable, total);
        } catch (Exception e) {

        }
        return pageNews;
    }

}
