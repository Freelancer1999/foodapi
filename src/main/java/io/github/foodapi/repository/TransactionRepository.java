package io.github.foodapi.repository;

import io.github.foodapi.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{
    List<Transaction> getTransactionsByCustomerId(Long customerId);
    List<Transaction> getTransactionsByBookingId(Long bookingId);
    List<Transaction> getTransactionsByCustomerName(String customerName);
    List<Transaction> findAllTransactionsByDate(LocalDate date);
}
