package io.github.foodapi.repository;

import io.github.foodapi.models.OrderDetail;
import io.github.foodapi.models.OrderDetailPK;
import io.github.foodapi.payload.response.AnalysisResponse;
import io.github.foodapi.payload.response.OrderFoodResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long> {

    @Query("SELECT od.orderId, od.foodId, f.name, f.description, f.imageUrl," +
            "od.quantity, f.price " +
            " FROM OrderDetail od inner  join Food f on od.foodId = f.id " +
            "WHERE od.orderId = :orderId")
    List<Object[]> getOrderDetailByOrderId(Long orderId);


    List<OrderDetail> findAllByOrOrderId(Long id);

    @Query(value = "SELECT f.name, SUM(od.price) FROM order_detail od " +
            "JOIN food f ON od.food_id = f.id  GROUP BY od.food_id", nativeQuery = true)
    List<Object[]> getPriceByFood();

    @Query(value = "select year(b.checkin) as year,month(b.checkin) as month ,sum(od.price) as total_price\n" +
            " FROM order_detail od INNER JOIN bookings b\n" +
            " group by year(b.checkin),month(b.checkin)\n" +
            " order by year(b.checkin) asc ,month(b.checkin) asc", nativeQuery = true)
    List<Object[]> getAnalysisOrder();

    @Query(value = "select year(b.checkin) as year,month(b.checkin) as month , day(b.checkin) as day,sum(od.price) as total_price\n" +
            "FROM order_detail od \n" +
            "INNER JOIN bookings b " +
            " WHERE b.checkin BETWEEN CURDATE() - INTERVAL 30 DAY AND NOW() \n" +
            "group by year(b.checkin),month(b.checkin),day(b.checkin)\n" +
            "order by year(b.checkin) desc,month(b.checkin) desc,day(b.checkin) desc", nativeQuery = true)
    List<Object[]> getAnalysisOrderByDay();

    @Query(value = "SELECT o.id as order_id, c.name, o.total_price, o.order_time " +
            "FROM customers c INNER JOIN orders o on c.id = o.customer_id\n" +
            "WHERE o.order_time >=  (CURDATE() - INTERVAL 1 MONTH )\n" +
            "ORDER BY o.total_price desc", nativeQuery = true)
    List<Object[]> getTotalPriceForCustomerByMonth();
}
