package io.github.foodapi.repository;

import io.github.foodapi.models.Booking;
import io.github.foodapi.models.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends JpaRepository<Food, Long> {

    @Query(value = "SELECT * FROM food WHERE category_id = ?", nativeQuery = true)
    List<Food> findFoodByCategoryId(Long categoryId);

    List<Food> findFoodById(Long id);

}
