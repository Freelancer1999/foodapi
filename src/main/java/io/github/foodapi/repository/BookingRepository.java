package io.github.foodapi.repository;

import io.github.foodapi.models.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>, BookingRepositoryCustom{

    List<Booking> getBookingByDateTimeAndCustomerId(String checkin, String checkout, Long customerId);

    List<Booking> getBookingsByCustomerId(Long customerId);

    @Query(value = "SELECT * FROM booking_table INNER JOIN bookings ON booking_table.booking_id = bookings.id " +
            "INNER JOIN restaurant_tables ON \n" +
            "  booking_table.table_id = restaurant_tables.id INNER JOIN customers c on bookings.customer_id = c.id  " +
            "WHERE table_id=?1 " +
            "ORDER BY bookings.checkin DESC LIMIT 1", nativeQuery = true)
    Booking findByBookingByTableId(Long tableId);

    @Query(value = "SELECT * FROM bookings INNER JOIN customers " +
            "ON bookings.customer_id = customers.id  WHERE customer_id = 1", nativeQuery = true)
    List<Booking> findByBookingByCustomerId(Long customerId);

    @Query(value = "SELECT * FROM  bookings order by checkin desc", nativeQuery = true)
    List<Booking> getAllBookingOrderByCheckin();

    @Transactional
    @Modifying
    @Query(value = "UPDATE bookings t SET t.is_order = true WHERE t.customer_id = ?1", nativeQuery = true)
    void updateStatusOrder(Long customerId);
}
