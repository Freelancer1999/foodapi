package io.github.foodapi.services;

import io.github.foodapi.models.User;
import io.github.foodapi.payload.request.UserRequest;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    User createUser(UserRequest userRequest, UserPrinciple currentUser);

    Optional<User> getUser(Long id);

    User editUser(Long id, User editUser, UserPrinciple currentUser);

    void deleteUser(Long id, UserPrinciple currentUser);

    Optional<User> findByUsername(String name); //Tim kiem User co ton tai trong DB khong?

    Boolean existsByUsername(String username); //username da co trong DB chua, khi tao du lieu

    Boolean existsByEmail(String email); //email da co trong DB chua

    User save(User user);
}
