package io.github.foodapi.services;

import io.github.foodapi.models.Order;
import io.github.foodapi.payload.request.OrderRequest;
import io.github.foodapi.payload.response.OrderBookingResponse;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    List<Order> getAllOrders();

    Optional<Order> getOrder(Long id);

    Order createOrder(OrderRequest newOrder, UserPrinciple currentUser);

    Order editOrder(Long id, Order editOrder, UserPrinciple currentUser);

    void deleteOrder(Long id, UserPrinciple currentUser);

    List<OrderBookingResponse> findByOrder();

    Order findOrderByCustomer(Long customerId);

    Integer sumQuantityTotalOrder(Long orderId);

    int sumPriceTotalOrder(Long customerId);

    int sumTotalPriceByCustomerId(Long orderId);

}
