package io.github.foodapi.services;

import io.github.foodapi.models.Category;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAllCategories();

    Optional<Category> getCategory(Long id);

    Category createCategory(Category newCategory, UserPrinciple currentUser);

    Category editCategory(Long id, Category editCategory, UserPrinciple currentUser);

    void deleteCategory(Long id,  UserPrinciple currentUser);


}
