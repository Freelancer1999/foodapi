package io.github.foodapi.services;

import io.github.foodapi.models.Transaction;

import java.util.List;

public interface TransactionService {

    List<Transaction> getAllTransactions();

    List<Transaction> getAllTransactionsForDate(String date);

    List<Transaction> getTransactionsByBookingId(Long bookingId);

    List<Transaction> getTransactionsByCustomerId( Long customerId);

    List<Transaction> getTransactionsByCustomerName(String customerName);
}
