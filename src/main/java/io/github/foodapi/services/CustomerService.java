package io.github.foodapi.services;

import io.github.foodapi.models.Customer;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomers();

    List<Customer> getCustomerByName(String name);

    List<Customer> getCustomerById(Long id);

    List<Customer> getAllCustomersOrderByCountBookings();

    Customer creatCustomer(Customer newCustomer, UserPrinciple currentUser);

    Customer editCustomer(Long id, Customer editCustommer, UserPrinciple currentUser);

}
