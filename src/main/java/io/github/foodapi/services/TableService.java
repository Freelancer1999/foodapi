package io.github.foodapi.services;

import io.github.foodapi.models.RestaurantTable;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface TableService {
    List<RestaurantTable> getAllRestaurantTables();

    List<RestaurantTable> getTablesBySeating(int number);

    List<RestaurantTable> getTablesAvailableOnDate(String date);

    Optional<RestaurantTable> getTable(Long id);

    RestaurantTable createTable(RestaurantTable newTable, UserPrinciple currentUser);

    RestaurantTable editTable(Long id, RestaurantTable editTable, UserPrinciple currentUser);

    void deleteTable(Long id, UserPrinciple currentUser);

    void updateTableStatus(Boolean status, Long id);

}
