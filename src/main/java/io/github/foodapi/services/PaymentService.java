package io.github.foodapi.services;

import io.github.foodapi.models.Payment;

import java.util.List;

public interface PaymentService {
    List<Payment> findALl();

    Payment savePayment(Payment payment);

}
