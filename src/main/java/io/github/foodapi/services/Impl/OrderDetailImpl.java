package io.github.foodapi.services.Impl;


import io.github.foodapi.models.Food;
import io.github.foodapi.models.OrderDetail;
import io.github.foodapi.payload.request.AddFoodRequest;
import io.github.foodapi.payload.response.*;
import io.github.foodapi.repository.FoodRepository;
import io.github.foodapi.repository.OrderDetailRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderDetailImpl implements OrderDetailService {

    @Autowired
    OrderDetailRepository orderDetailRepository;

    @Autowired
    FoodRepository foodRepository;



    @Override
    public List<OrderFoodResponse> getOrderDetails(long orderId) {
       List<Object[]> list = orderDetailRepository.getOrderDetailByOrderId(orderId);
       List<OrderFoodResponse> foodResponseList = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            OrderFoodResponse orderFoodResponse = null;
            for (Object[] object : list) {
                orderFoodResponse = new OrderFoodResponse();
                orderFoodResponse.setOrderId(Long.parseLong(object[0].toString()));
                orderFoodResponse.setFoodId(Long.parseLong(object[1].toString()));
                orderFoodResponse.setFoodName(object[2].toString());
                orderFoodResponse.setDescription(object[3].toString());
                orderFoodResponse.setImageUrl(object[4].toString());
                orderFoodResponse.setQuantity(Long.parseLong(object[5].toString()));
                orderFoodResponse.setPrice(Double.parseDouble(object[6].toString()));

                foodResponseList.add(orderFoodResponse);
            }
        }
        return foodResponseList;
    }

    @Override
    public List<OrderDetail> createOrderDetail(AddFoodRequest orderDetail, UserPrinciple currentUser) {
        List<OrderDetail> orderDetails = orderDetail.getListFoods();
        for (OrderDetail order : orderDetails) {
            List<OrderDetail> listOrderDetails = orderDetailRepository.findAllByOrOrderId(order.getOrderId());
            List<Food> foodLists = foodRepository.findFoodById(order.getFoodId());

            for (OrderDetail listOrder : listOrderDetails) {
                if( listOrder.getFoodId() == order.getFoodId()) {
                    order.setQuantity(listOrder.getQuantity() + order.getQuantity());
                    for (Food listFood : foodLists) {
                           if (listFood.getId() != null && order.getOrderId() != null) {
                               order.setPrice(listFood.getPrice() * order.getQuantity());
                           }
                    }
                }
            }
        }

            return  orderDetailRepository.saveAll(orderDetails);
    }

    @Override
    public List<AnalysisResponse> getPriceByFood() {
        List<Object[]> list = orderDetailRepository.getPriceByFood();
        List<AnalysisResponse> analysisResponses = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            AnalysisResponse analysisResponse = null;
            for (Object[] object : list) {
                analysisResponse = new AnalysisResponse();
                analysisResponse.setFoodName(object[0].toString());
                analysisResponse.setTotalPrice(Double.parseDouble(object[1].toString()));

                analysisResponses.add(analysisResponse);
            }
        }
        return analysisResponses;
    }

    @Override
    public List<AnalysisOrderResponse> getAnalysisOrder() {
        List<Object[]> list = orderDetailRepository.getAnalysisOrder();
        List<AnalysisOrderResponse> results = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            AnalysisOrderResponse analysisResponse = null;
            for (Object[] object : list) {
                analysisResponse = new AnalysisOrderResponse();
                analysisResponse.setYear(object[0].toString());
                analysisResponse.setMonth(object[1].toString());
                analysisResponse.setTotalPrice(Double.parseDouble(object[2].toString()));

                results.add(analysisResponse);
            }
        }
        return results;
    }

    @Override
    public List<AnalysisOrderByDayResponse> getAnalysisOrderByDay() {
        List<Object[]> list = orderDetailRepository.getAnalysisOrderByDay();
        List<AnalysisOrderByDayResponse> results = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            AnalysisOrderByDayResponse analysisOrderByDayResponse = null;
            for (Object[] object : list) {
                analysisOrderByDayResponse = new AnalysisOrderByDayResponse();
                analysisOrderByDayResponse.setYear(object[0].toString());
                analysisOrderByDayResponse.setMonth(object[1].toString());
                analysisOrderByDayResponse.setDay(object[2].toString());
                analysisOrderByDayResponse.setTotalPrice(Double.parseDouble(object[3].toString()));

                results.add(analysisOrderByDayResponse);
            }
        }
        return results;
    }

    @Override
    public List<AnalysisTotalPriceByCustomer> getAnalysisTotalPriceForCustomerByMonth() {
        List<Object[]> list = orderDetailRepository.getTotalPriceForCustomerByMonth();
        List<AnalysisTotalPriceByCustomer> results = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            AnalysisTotalPriceByCustomer listItem = null;
            for (Object[] object : list) {
                listItem = new AnalysisTotalPriceByCustomer();
                listItem.setOrderId(Long.parseLong(object[0].toString()));
                listItem.setCustomerName(object[1].toString());
                listItem.setTotalPrice(Double.parseDouble(object[2].toString()));
                listItem.setOrderTime(Long.parseLong(object[3].toString()));

                results.add(listItem);
            }
        }
        return results;
    }


}
