package io.github.foodapi.services.Impl;

import io.github.foodapi.models.Order;
import io.github.foodapi.models.Payment;
import io.github.foodapi.repository.OrderRepository;
import io.github.foodapi.repository.PaymentRepository;
import io.github.foodapi.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public List<Payment> findALl() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment savePayment(Payment payment) {
        return paymentRepository.save(payment);
    }


}
