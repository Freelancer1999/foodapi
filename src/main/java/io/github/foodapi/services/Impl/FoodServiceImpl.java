package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.models.Food;
import io.github.foodapi.payload.request.FoodRequest;
import io.github.foodapi.repository.FoodRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.Optional;

@Service
public class FoodServiceImpl implements FoodService {

    private static final Path CURRENT_FOLDER = Paths.get(System.getProperty("user.dir"));

    @Autowired
    FoodRepository foodRepository;

    @Override
    public List<Food> getAllFoods() {
        return foodRepository.findAll();
    }

    @Override
    public List<Food> getFoodByCategoryId(Long id) {
        return foodRepository.findFoodByCategoryId(id);
    }

    @Override
    public Optional<Food> getFood(Long id) {
        return foodRepository.findById(id);
    }

    @Override
    public Food createFood(FoodRequest newFood, UserPrinciple currentUser) {
        Food food = new Food();
        food.setName(newFood.getName());
        food.setDescription(newFood.getDescription());
        food.setPrice(newFood.getPrice());
        food.setStatus(true);
        food.setImageUrl(newFood.getImageURL());
        food.setCategory(newFood.getCategory());
        return foodRepository.save(food);
    }

    @Override
    public Food editFood(Long id, FoodRequest editFood, UserPrinciple currentUser) {
        return foodRepository.findById(id)
                .map(food -> {
                    food.setName(editFood.getName());
                    food.setDescription(editFood.getDescription());
                    food.setPrice(editFood.getPrice());
                    food.setImageUrl(editFood.getImageURL());
                    food.setCategory(editFood.getCategory());
                    food.setStatus(editFood.isStatus());
                    return food;
                })
                .map(foodRepository::save)
                .orElseThrow(
                        () -> new ResourceNotFoundException("FOod Id " + id + " not found")
                );
    }

    @Override
    public void deleteFood(Long id, UserPrinciple currentUser) {
        foodRepository.findById(id)
                .map(food -> {
                    foodRepository.delete(food);
                    return ResponseEntity
                            .ok()
                            .build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("Food ID " + id + " not found")
                );
    }
}
