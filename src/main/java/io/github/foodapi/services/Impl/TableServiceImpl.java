package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.exception.UnauthorizedException;
import io.github.foodapi.models.ERole;
import io.github.foodapi.models.RestaurantTable;
import io.github.foodapi.repository.RestaurantTableRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.TableService;
import io.github.foodapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import static io.github.foodapi.utils.AppConstants.TABLE;
import static io.github.foodapi.utils.AppConstants.ID;

import java.util.List;
import java.util.Optional;

@Service
public class TableServiceImpl implements TableService {

    @Autowired
    RestaurantTableRepository tableRepository;

    @Override
    public List<RestaurantTable> getAllRestaurantTables() {
        return tableRepository.findAll();
    }

    @Override
    public List<RestaurantTable> getTablesBySeating(int number) {
        return tableRepository.findTablesBySeating(number);
    }

    @Override
    public List<RestaurantTable> getTablesAvailableOnDate(String date) {
        return tableRepository.getTablesAvailableOnDate(date);
    }

    @Override
    public Optional<RestaurantTable> getTable(Long id) {
        return tableRepository.findById(id);
    }

    @Override
    public RestaurantTable createTable(RestaurantTable newTable, UserPrinciple currentUser) {
        return tableRepository.save(newTable);
    }

    @Override
    public RestaurantTable editTable(Long id, RestaurantTable editTable, UserPrinciple currentUser) {
        RestaurantTable table = tableRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(TABLE, ID, id));

        if (table.getId().equals(currentUser.getId())
                || currentUser.getAuthorities().contains((new SimpleGrantedAuthority(ERole.ROLE_ADMIN.toString())))) {
            table.setTableNumber(editTable.getTableNumber());
            table.setSeating(editTable.getSeating());
            table.setStatus(editTable.getStatus());

            return tableRepository.save(table);
        }

        ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "You don't have permission to update profile of:" + id);
        throw  new UnauthorizedException(apiResponse);
    }


    @Override
    public void deleteTable(Long id, UserPrinciple currentUser) {
//        tableRepository.deleteById(id);
        this.tableRepository.findById(id)
                .map(table -> {
                    tableRepository.delete(table);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("table Id" + id + "not found"));
    }

    @Override
    public void updateTableStatus(Boolean status, Long id) {
        tableRepository.setStatusTable(status, id);
    }


}
