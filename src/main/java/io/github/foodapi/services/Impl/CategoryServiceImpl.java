package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.models.Category;
import io.github.foodapi.repository.CategoryRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> getCategory(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public Category createCategory(Category newCategory, UserPrinciple currentUser) {
        return categoryRepository.save(newCategory);
    }

    @Override
    public Category editCategory(Long id, Category editCategory, UserPrinciple currentUser) {
        return categoryRepository.findById(id)
                .map(category -> {
                            category.setName(editCategory.getName());
                            category.setStatus(editCategory.isStatus());
                            return category;
                        }
                )
                .map(categoryRepository::save)
                .orElseThrow(
                        () -> new ResourceNotFoundException("Category Id " + id + " not found")
                );
    }

    @Override
    public void deleteCategory(Long id, UserPrinciple currentUser) {
        categoryRepository.findById(id)
                .map(category -> {
                    categoryRepository.delete(category);
                    return ResponseEntity
                            .ok()
                            .build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("Category ID " + id + " not found")
                );
    }
}
