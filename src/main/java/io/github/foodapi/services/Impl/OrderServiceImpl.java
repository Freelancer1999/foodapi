package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.models.Order;
import io.github.foodapi.payload.request.OrderRequest;
import io.github.foodapi.payload.response.OrderBookingResponse;
import io.github.foodapi.repository.CustomerRepository;
import io.github.foodapi.repository.OrderRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class  OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> getOrder(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    public Order createOrder(OrderRequest newOrder, UserPrinciple currentUser) {
        Order order = new Order();
        order.setStatus(true);
        order.setOrderTime(System.currentTimeMillis());
        //order.setTotalPrice(newOrder.getFoods().stream().mapToDouble(e -> (e.getPrice() * 1)).sum());
        order.setCustomer(customerRepository.save(newOrder.getCustomer()));
        order.setFoods(newOrder.getFoods());

        return orderRepository.save(order);
    }

    @Override
    public Order editOrder(Long id, Order editOrder, UserPrinciple currentUser) {
        return  orderRepository.findById(id)
                .map(
                        order -> {
                            order.setTotalPrice(editOrder.getTotalPrice());
                            order.setOrderTime(System.currentTimeMillis());
                            order.setStatus(true);
                            return order;
                        }
                )
                .map(orderRepository::save)
                .orElseThrow(
                        () -> new ResourceNotFoundException("order id " + id + " not found")
                );
        }


    @Override
    public void deleteOrder(Long id, UserPrinciple currentUser) {
        orderRepository.findById(id)
                .map(order -> {
                    orderRepository.delete(order);
                    return ResponseEntity
                            .ok()
                            .build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("Order ID " + id + " not found")
                );

    }

    @Override
    public List<OrderBookingResponse> findByOrder() {
        List<Object[]> list = orderRepository.findByOrder();
        List<OrderBookingResponse> orderBookingResponses = new ArrayList<>();
        if (list != null  && !list.isEmpty()) {
            OrderBookingResponse orderBookingResponse = null;
            for (Object[] object : list) {
                orderBookingResponse = new OrderBookingResponse();
                orderBookingResponse.setOrderId(Long.parseLong(object[0].toString()));
                orderBookingResponse.setOrderTime(Long.parseLong(object[1].toString()));
                orderBookingResponse.setCustomerName(object[2].toString());
                orderBookingResponse.setCustomerPhone(object[3].toString());
                orderBookingResponse.setNotes(object[4].toString());
                orderBookingResponse.setNumberInParty(Integer.parseInt(object[5].toString()));

                orderBookingResponses.add(orderBookingResponse);
            }
        }
        return orderBookingResponses;
    }

    @Override
    public Order findOrderByCustomer(Long customerId) {
        Order orders =  orderRepository.findByOrderByCustomer(customerId);
//        if (orders != null ) {
//            orderRepository.sumTotalPriceByCustomerId(customerId);
//        }
        return orders;
    }

    @Override
    public Integer sumQuantityTotalOrder(Long orderId) {
        return orderRepository.sumQuantityTotalOrder(orderId);
    }

    @Override
    public int sumPriceTotalOrder(Long customerId) {
        return orderRepository.sumPriceTotalOrder(customerId);
    }

    @Override
    public int sumTotalPriceByCustomerId(Long customerId) {
        return orderRepository.sumTotalPriceByCustomerId(customerId);
    }
}
