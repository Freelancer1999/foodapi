package io.github.foodapi.services.Impl;

import io.github.foodapi.models.DTO.NewsSearch;
import io.github.foodapi.models.News;
import io.github.foodapi.repository.NewsRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.NewsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsRepository newsRepository;


    @Override
    public Page<News> findListNews(NewsSearch newsSearch) {
        Pageable pageable;
        if (newsSearch.getSort() == null || "".equals(newsSearch.getSort())){
            pageable = PageRequest.of(newsSearch.getPage(), newsSearch.getSize(), Sort.by("id").ascending());
        } else {
            pageable = newsSearch.isAsc() ?
                    PageRequest.of(newsSearch.getPage(), newsSearch.getSize(), Sort.by(newsSearch.getSort()).ascending())
                    : PageRequest.of(newsSearch.getPage(), newsSearch.getSize(), Sort.by(newsSearch.getSort()).descending());
        }

        News news = new News();
        BeanUtils.copyProperties(news, newsSearch);
        Page<News> newsList = newsRepository.findAll(Example.of(news), pageable);

        return newsList;

    }

    @Override
    public void addNews(News newsRequest, UserPrinciple currentUser) {
        News news = new News();
        news.setTitle(newsRequest.getTitle());
        news.setBody(newsRequest.getBody());
        news.setImageUrl(newsRequest.getImageUrl());
        news.setUserId(null);
        newsRepository.save(news);
    }
}
