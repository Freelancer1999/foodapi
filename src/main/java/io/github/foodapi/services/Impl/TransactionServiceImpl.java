package io.github.foodapi.services.Impl;

import io.github.foodapi.models.Transaction;
import io.github.foodapi.repository.TransactionRepository;
import io.github.foodapi.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    @Override
    public List<Transaction> getAllTransactionsForDate(String date) {
        LocalDate getDate = LocalDate.parse(date);
        return transactionRepository.findAllTransactionsByDate(getDate);
    }

    @Override
    public List<Transaction> getTransactionsByBookingId(Long bookingId) {
        return transactionRepository.getTransactionsByBookingId(bookingId);
    }

    @Override
    public List<Transaction> getTransactionsByCustomerId(Long customerId) {
        return transactionRepository.getTransactionsByCustomerId(customerId);
    }

    @Override
    public List<Transaction> getTransactionsByCustomerName(String customerName) {
        return transactionRepository.getTransactionsByCustomerName(customerName);
    }
}
