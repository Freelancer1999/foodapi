package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.LoginFailedException;
import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.exception.UnauthorizedException;
import io.github.foodapi.models.ERole;
import io.github.foodapi.models.Role;
import io.github.foodapi.models.User;
import io.github.foodapi.payload.request.UserRequest;
import io.github.foodapi.repository.RoleRepository;
import io.github.foodapi.repository.UserRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.UserService;
import io.github.foodapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static io.github.foodapi.utils.AppConstants.ID;
import static io.github.foodapi.utils.AppConstants.USER;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

//    @Override
//    public List<User> getAllUsers(int limit, int offset) {
//        Pageable pageable = new OffsetBasedPageRequest(limit, offset);
//        return userRepository.findAll(pageable).getContent();
//    }

    @Override
    public Optional<User> getUser(Long id) {
        return this.userRepository.findById(id);
    }


    @Override
    public User createUser(UserRequest userRequest, UserPrinciple currentUser) {
//        userRequest.setPassword(passwordEncoder.encode(userRequest.getPassword()));
//        userRequest.setStatus(true);
//
//        Set<Role> strRoles = userRequest.getRoles();
//        Set<Role> roles = new HashSet<>();

        // Create new user's account
        User user = new User(userRequest.getUsername(),
                userRequest.getEmail(),
                passwordEncoder.encode(userRequest.getPassword()),
                userRequest.getName(),
                userRequest.getBirthday(),
                userRequest.getPhoneNumber(),
                userRequest.getAddress(),
                userRequest.getAvatar(),
                true);

        Set<String> strRoles = userRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "mod":
                        Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public User editUser(Long id, User editUser, UserPrinciple currentUser) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(USER, ID, id));
        if (user.getId().equals(currentUser.getId())
                || currentUser.getAuthorities().contains((new SimpleGrantedAuthority(ERole.ROLE_ADMIN.toString())))) {
            user.setUsername(editUser.getUsername());
            user.setName(editUser.getName());
            user.setPassword(passwordEncoder.encode(editUser.getPassword()));
            user.setEmail(editUser.getEmail());
            user.setAddress(editUser.getAddress());
            user.setBirthday(editUser.getBirthday());
            user.setPhoneNumber(editUser.getPhoneNumber());
            user.setStatus(editUser.getStatus());

            return userRepository.save(user);
        }
        ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "You don't have permission to update profile of:" + id);
        throw new UnauthorizedException(apiResponse);
    }

    @Override
    public void deleteUser(Long id, UserPrinciple currentUser) {
        this.userRepository.findById(id)
                .map(user -> {
                    userRepository.delete(user);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("User Id" + id + "not found"));
    }

    @Override
    public Optional<User> findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
}
