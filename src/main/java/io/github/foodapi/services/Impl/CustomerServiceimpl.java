package io.github.foodapi.services.Impl;

import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.exception.UnauthorizedException;
import io.github.foodapi.models.Customer;
import io.github.foodapi.models.ERole;
import io.github.foodapi.repository.CustomerRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.CustomerService;
import io.github.foodapi.utils.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Currency;
import java.util.List;

import static io.github.foodapi.utils.AppConstants.ID;
import static io.github.foodapi.utils.AppConstants.CUSTOMER;


@Service
public class CustomerServiceimpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public List<Customer> getCustomerByName(String name) {
        return customerRepository.findCustomerByName(name);
    }

    @Override
    public List<Customer> getCustomerById(Long id) {
        return customerRepository.findCustomerById(id);
    }

    @Override
    public List<Customer> getAllCustomersOrderByCountBookings() {
        return customerRepository.findAllByBookings();
    }

    @Override
    public Customer creatCustomer(Customer newCustomer, UserPrinciple currentUser) {
        Customer customer = new Customer();
        customer.setName(newCustomer.getName());
        customer.setPhone(newCustomer.getPhone());
        customer.setStatus(true);
        return customerRepository.save(customer);
    }

    @Override
    public Customer editCustomer(Long id, Customer editCustommer, UserPrinciple currentUser) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(CUSTOMER, ID, id));

        if (customer.getId().equals(currentUser.getId())
                || currentUser.getAuthorities().contains((new SimpleGrantedAuthority(ERole.ROLE_ADMIN.toString())))) {
            customer.setName(editCustommer.getName());
            customer.setPhone(editCustommer.getPhone());
            customer.setStatus(editCustommer.getStatus());

            return customerRepository.save(customer);
        }

        ApiResponse apiResponse = new ApiResponse(Boolean.FALSE, "You don't have permission to update profile of:" + id);
        throw  new UnauthorizedException(apiResponse);
    }
}
