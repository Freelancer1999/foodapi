package io.github.foodapi.services.Impl;


import io.github.foodapi.exception.ResourceNotFoundException;
import io.github.foodapi.models.Booking;
import io.github.foodapi.repository.BookingRepository;
import io.github.foodapi.repository.CustomerRepository;
import io.github.foodapi.security.services.UserPrinciple;
import io.github.foodapi.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<Booking> getAllBooking() {
        return bookingRepository.getAllBookingOrderByCheckin();
    }

    @Override
    public List<Booking> getBookingByDateTimeAndCustomerId(String checkin, String checkout, Long customerId) {
        return bookingRepository.getBookingByDateTimeAndCustomerId(checkin, checkout, customerId);
    }

    @Override
    public Optional<Booking> getBookingById(Long id, UserPrinciple currentUser) {
        return bookingRepository.findById(id);
    }

    @Transactional
    @Override
    public Booking createBooking(Booking newBooking, UserPrinciple currentUser) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date todayDate = Calendar.getInstance().getTime();
        String today = formatter.format(todayDate);

        Booking booking = new Booking();
        booking.setStatus(true);
        booking.setCheckin(today);
        booking.setNotes(newBooking.getNotes());
        booking.setNumberInParty(newBooking.getNumberInParty());
        booking.setUserCheckin(currentUser.getName());
        booking.setMethodType(newBooking.getMethodType());
        booking.setIsOrder(false);

        booking.setCustomer(customerRepository.save(newBooking.getCustomer()));
        booking.setRestaurantTables(newBooking.getRestaurantTables());

        return bookingRepository.save(booking);
    }

    @Override
    public Booking editBooking(Booking editBooking, Long id, UserPrinciple currentUser) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date todayDate = Calendar.getInstance().getTime();
        String today = formatter.format(todayDate);

        return bookingRepository.findById(id)
                .map(
                        booking -> {
                            booking.setCheckout(today);
                            booking.setUserCheckout(currentUser.getName());
                            booking.setStatus(false);
                            booking.setIsOrder(editBooking.getIsOrder());
                            booking.setNumberInParty(editBooking.getNumberInParty());
                            booking.setMethodType(editBooking.getMethodType());
                            booking.setRestaurantTables(editBooking.getRestaurantTables());
                            booking.setCustomer(editBooking.getCustomer());
                            return booking;
                        }
                )
                .map(bookingRepository::save)
                .orElseThrow(
                        () -> new ResourceNotFoundException("BookingID " + id + " not found")
                );
    }

    @Override
    public void deleteBooKing(Long id, UserPrinciple currentUser) {
        bookingRepository.findById(id)
                .map(booking -> {
                    bookingRepository.delete(booking);
                    return ResponseEntity
                            .ok()
                            .build();
                })
                .orElseThrow(() -> new ResourceNotFoundException("BookingID " + id + " not found"
                        )
                );
    }

    @Override
    public Booking findByBookingByTableId(Long tableId) {
        return bookingRepository.findByBookingByTableId(tableId);
    }

    @Override
    public List<Booking> findBookingByCustomerId(Long customerId) {
        return bookingRepository.findByBookingByCustomerId(customerId);
    }

    @Override
    public void updateStatusOrder(Long customerId) {
        bookingRepository.updateStatusOrder(customerId);
    }
}
