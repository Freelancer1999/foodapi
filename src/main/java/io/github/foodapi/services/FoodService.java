package io.github.foodapi.services;

import io.github.foodapi.models.Food;
import io.github.foodapi.payload.request.FoodRequest;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface FoodService {
    List<Food> getAllFoods();

    List<Food> getFoodByCategoryId(Long id);

    Optional<Food> getFood(Long id);

    Food createFood(FoodRequest newFood, UserPrinciple currentUser);

    Food editFood(Long id, FoodRequest editFood, UserPrinciple currentUser);

    void  deleteFood(Long id, UserPrinciple currentUser);
}
