package io.github.foodapi.services;

import io.github.foodapi.models.Booking;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Optional;

public interface BookingService {
    List<Booking> getAllBooking();

    List<Booking> getBookingByDateTimeAndCustomerId(String checkin, String checkout, Long customerId);

    Optional<Booking> getBookingById(Long id, UserPrinciple currentUser);

    Booking createBooking(Booking newBooking, UserPrinciple currentUser);

    Booking editBooking(Booking editBooking, Long id, UserPrinciple currentUser);

    void deleteBooKing(Long id, UserPrinciple currentUser);

    Booking findByBookingByTableId(Long tableId);

    List<Booking> findBookingByCustomerId(Long customerId);

    void updateStatusOrder(Long customerId);
}
