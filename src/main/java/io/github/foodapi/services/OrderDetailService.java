package io.github.foodapi.services;

import io.github.foodapi.models.OrderDetail;
import io.github.foodapi.payload.request.AddFoodRequest;
import io.github.foodapi.payload.response.*;
import io.github.foodapi.security.services.UserPrinciple;

import java.util.List;
import java.util.Map;

public interface OrderDetailService {
    List<OrderFoodResponse> getOrderDetails(long orderId);

    List<OrderDetail> createOrderDetail(AddFoodRequest orderDetail, UserPrinciple currentUser);

    List<AnalysisResponse> getPriceByFood();

    List<AnalysisOrderResponse> getAnalysisOrder();

    List<AnalysisOrderByDayResponse> getAnalysisOrderByDay();

    List<AnalysisTotalPriceByCustomer> getAnalysisTotalPriceForCustomerByMonth();
}
