package io.github.foodapi.services;

import io.github.foodapi.models.DTO.NewsSearch;
import io.github.foodapi.models.News;
import io.github.foodapi.security.services.UserPrinciple;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NewsService {
    Page<News> findListNews(NewsSearch newsSearch);

    void addNews(News newsRequest, UserPrinciple currentUser);



}
