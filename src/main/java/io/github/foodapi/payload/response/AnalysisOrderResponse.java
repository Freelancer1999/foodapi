package io.github.foodapi.payload.response;

import lombok.Data;

@Data
public class AnalysisOrderResponse {
    private String year;
    private String month;
    private Double totalPrice;
}
