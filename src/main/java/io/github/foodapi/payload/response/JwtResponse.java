package io.github.foodapi.payload.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String name;
	private String email;
	private String avatar;
	private List<String> roles;

	public JwtResponse(Long id,String name, String username, String email, String avatar, List<String> roles,String accessToken) {
		this.id = id;
		this.name = name;
		this.username = username;
		this.email = email;
		this.avatar = avatar;
		this.roles = roles;
		this.token = accessToken;
	}
}
