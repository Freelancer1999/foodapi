package io.github.foodapi.payload.response;

import lombok.Data;

@Data
public class AnalysisResponse {
    private String foodName;
    private Double totalPrice;
}
