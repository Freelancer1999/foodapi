package io.github.foodapi.payload.response;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderBookingResponse {
    private Long orderId;
    private long orderTime;
    private String customerName;
    private String  customerPhone;
    private String notes;
    private Integer numberInParty;

    public OrderBookingResponse() {
    }

    public OrderBookingResponse(Long orderId, long orderTime, String customerName, String customerPhone, String notes, Integer numberInParty) {
        this.orderId = orderId;
        this.orderTime = orderTime;
        this.customerName = customerName;
        this.customerPhone = customerPhone;
        this.notes = notes;
        this.numberInParty = numberInParty;
    }
}
