package io.github.foodapi.payload.response;

import io.github.foodapi.models.Customer;
import io.github.foodapi.models.Food;
import io.github.foodapi.models.OrderDetail;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderResponse {

    double totalPrice;
    long orderTime;
    boolean status;
    List<OrderDetail> listFoods;
    Customer customer;

}
