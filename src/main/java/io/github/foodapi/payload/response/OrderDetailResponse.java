package io.github.foodapi.payload.response;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;


@Data
public class OrderDetailResponse {
    private Long orderId;
    private Long foodId;
}
