package io.github.foodapi.payload.response;

import lombok.Data;

@Data
public class AnalysisOrderByDayResponse {
        private String year;
        private String month;
        private String day;
        private Double totalPrice;
}
