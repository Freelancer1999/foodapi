package io.github.foodapi.payload.response;


public class OrderFoodResponse {
    private Long orderId;
    private Long foodId;
    private String foodName;
    private String  description;
    private String  imageUrl;
    private Long quantity;
    private Double price;

    public OrderFoodResponse() {
    }

    public OrderFoodResponse(Long orderId, Long foodId, String foodName, String description, String imageUrl, Long quantity, Double price) {
        this.orderId = orderId;
        this.foodId = foodId;
        this.foodName = foodName;
        this.description = description;
        this.imageUrl = imageUrl;
        this.quantity = quantity;
        this.price = price;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderFoodResponse{" +
                "orderId=" + orderId +
                ", foodId=" + foodId +
                ", foodName='" + foodName + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
