package io.github.foodapi.payload.response;

import lombok.Data;

@Data
public class AnalysisTotalPriceByCustomer {
    private Long orderId;
    private String customerName;
    private Double totalPrice;
    private Long orderTime;
}
