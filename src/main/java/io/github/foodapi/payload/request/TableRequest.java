package io.github.foodapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TableRequest {
    private String tableNumber;
    private int seating;
    private String location;
    private String posivition;
    private boolean status;

}
