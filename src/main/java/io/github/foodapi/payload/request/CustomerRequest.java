package io.github.foodapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CustomerRequest {
    private String name;
    private String phone;
    private boolean status;
}
