package io.github.foodapi.payload.request;

import io.github.foodapi.models.OrderDetail;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.List;

@Setter
@Getter
public class AddFoodRequest {
    List<OrderDetail> listFoods;

}
