package io.github.foodapi.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class BookingRequest {
    private String notes;
    private int numberInParty;
    private CustomerRequest customer;
    private List<TableRequest> tables;
}
