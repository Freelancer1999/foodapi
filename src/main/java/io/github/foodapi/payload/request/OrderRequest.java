package io.github.foodapi.payload.request;

import io.github.foodapi.models.Customer;
import io.github.foodapi.models.Food;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class OrderRequest {
    private List<Food> foods;
    private Customer customer;

}
