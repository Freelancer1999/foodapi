package io.github.foodapi.payload.request;

import io.github.foodapi.models.Category;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
public class FoodRequest {
    private String name;
    private String description;
    private double price;
    private String imageURL;
    private Category category;
    private boolean status;
}
