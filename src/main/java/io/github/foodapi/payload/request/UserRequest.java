package io.github.foodapi.payload.request;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class UserRequest {
    private String name;
    private String username;
    private String email;
    private String password;
    private LocalDate birthday;
    private String phoneNumber;
    private String address;
    private String avatar;
    private Set<String> role;

    private boolean status;

    public UserRequest() {
    }



}
