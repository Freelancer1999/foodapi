package io.github.foodapi.payload.request;

import lombok.Data;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Data
public class ChangePasswordRequest {
    @NotBlank
    private String username;

    private String currentPassword;

    @Size(min = 6, max = 20)
    private String password;
}

