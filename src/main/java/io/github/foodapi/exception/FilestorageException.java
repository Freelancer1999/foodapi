package io.github.foodapi.exception;

public class FilestorageException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private FilestorageException(String message) {
        super(message);
    }

    public FilestorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
