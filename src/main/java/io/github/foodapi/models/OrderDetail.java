package io.github.foodapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "order_detail")
@IdClass(OrderDetailPK.class)
public class OrderDetail implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "order_id",insertable = false, unique = false, updatable = false, nullable = false)
    private Long orderId;

    @Id
    @Column(name = "food_id",insertable = false, unique = false, updatable = false, nullable = false)
    private Long foodId;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "price")
    private Double price;

}
