package io.github.foodapi.models;

import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "News model")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "news")
public class News extends BaseEntity implements Serializable {

    @Column(name = "title")
    private String title;

    @Column(name = "body")
    private String body;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "create_date")
    private Date createDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private User userId;
}
