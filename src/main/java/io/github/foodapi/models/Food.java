package io.github.foodapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data // lombok giúp generate các hàm constructor, get, set v.v.
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "food")
public class Food extends BaseEntity implements Serializable {


    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name="price")
    private double price;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "status")
    private boolean status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Category category;

    @JsonIgnore
    @ManyToMany(mappedBy = "foods", cascade = CascadeType.ALL)
    private List<Order> orders;


}
