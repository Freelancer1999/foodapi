package io.github.foodapi.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payments")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Data
public class Payment extends BaseEntity implements Serializable {

    @Column(name = "title")
    private String title;

    @Column(name = "method_type")
    private int methodType;

    @Column(name = "account_name")
    private String accountName;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "account_number")
    private String accountNumber;

}
