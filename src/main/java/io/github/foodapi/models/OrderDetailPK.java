package io.github.foodapi.models;

import lombok.Data;


import java.io.Serializable;

@Data
public class OrderDetailPK implements Serializable {
    private Long orderId;
    private Long foodId;
}
