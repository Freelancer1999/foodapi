package io.github.foodapi.models.DTO;

import io.github.foodapi.models.News;
import io.github.foodapi.models.User;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class NewsSearch extends News {
    int page;
    String sort;
    int size;
    boolean asc;

    public NewsSearch(String title, String body, String imageUrl, Date createDate, User userId, int page, String sort, int size, boolean asc) {
        super(title, body, imageUrl, createDate, userId);
        this.page = page;
        this.sort = sort;
        this.size = size;
        this.asc = asc;
    }
}
