package io.github.foodapi.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Data // lombok giúp generate các hàm constructor, get, set v.v.
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "restaurantTables")
public class RestaurantTable implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name= "table_number")
    private String tableNumber;

    @Column(name = "seating")
    private int seating;

    @Column(name = "location")
    private String location;

    @Column(name = "posivition")
    private String posivition;

    @Column(name = "status",columnDefinition="tinyint(1) default 1")
    private Boolean status;

    @JsonIgnore
    @ManyToMany(mappedBy = "restaurantTables", cascade = CascadeType.ALL)
    private List<Booking> bookings;
}
